package com.kshrd.homework003;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

public class AnimationActivity extends AppCompatActivity {
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation);

        imageView=findViewById(R.id.imageView);
        Bundle bundle=getIntent().getExtras();

        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),bundle.getInt("animate"));
//        imageView.setImageResource(R.drawable.logo_edu);
        imageView.startAnimation(animation);

    }
}