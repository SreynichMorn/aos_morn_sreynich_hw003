package com.kshrd.homework003;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.PriorityQueue;

public class MainActivity extends AppCompatActivity {
    private AlertDialog.Builder builder;
    private AlertDialog dialog;
    private EditText title,description;
    private Button cancel,save;
    ListView listView;
    ArrayList<String>arrayList;
    ArrayAdapter<String>adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.options_menu,menu);
        title=findViewById(R.id.editTitle);
        description=findViewById(R.id.editDescription);
        cancel=findViewById(R.id.btnCancel);


//        bg_popup.setAlpha(0);
//        popup.setAlpha(0);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent=new Intent(this,AnimationActivity.class);
        final View addPopupView=getLayoutInflater().inflate(R.layout.popup,null);
        switch (item.getItemId()) {
            case R.id.add_item:
                addListDialog();
                return true;
            case R.id.remove_all_item:
                clearListView();
                Toast.makeText(this, "Delete All Item", Toast.LENGTH_LONG).show();
                return true;
            case R.id.add_anim:
                Toast.makeText(this, "Animation", Toast.LENGTH_LONG).show();
                return true;
            case R.id.fade_in_item:
                intent.putExtra("animate",R.anim.fade_in_anim);
                startActivity(intent);
                return true;
            case R.id.fade_out_item:
                intent.putExtra("animate",R.anim.fade_out_anim);
                startActivity(intent);
                return true;
            case R.id.zoom_item:
                intent.putExtra("animate",R.anim.zoom_anim);
                startActivity(intent);
                return true;
            case R.id.rotate_item:
                intent.putExtra("animate",R.anim.rotate_anim);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public PopupWindow addListDialog() {
        LayoutInflater inflater = (LayoutInflater)     getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.popup, null);
        builder = new AlertDialog.Builder(this);
        PopupWindow optionspu = new PopupWindow(layout);

        optionspu.setWidth(FrameLayout.LayoutParams.WRAP_CONTENT);
        optionspu.setHeight(FrameLayout.LayoutParams.WRAP_CONTENT);
        optionspu.setFocusable(true);
        optionspu.setOutsideTouchable(false);
        listView = findViewById(R.id.listView);
        arrayList = new ArrayList<String>();
        title = layout.findViewById(R.id.editTitle);
        description = layout.findViewById(R.id.editDescription);
        save = layout.findViewById(R.id.btnSave);
        cancel=layout.findViewById(R.id.btnCancel);

        builder.setView(layout);
        dialog = builder.create();
        dialog.show();


        save.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                //define save button here
                if (!title.getText().toString().isEmpty() && !description.getText().toString().isEmpty()) {
                    arrayList.add(title.getText().toString());
                    arrayList.add(description.getText().toString());
                    adapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_list_item_1, arrayList);
                    listView.setAdapter(adapter);
                } else {
                    title.setError("Enter Title");
                    description.setError("Enter Content");
                }


            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                title.setText("");
                description.setText("");
            }
        });
    return optionspu;

    }

    public void clearListView(){
        arrayList.clear();
        adapter.notifyDataSetChanged();
    }


}